var winState = { 
    create: function() {
        // Add a background image 
        // Display the name of the game 
        var nameLabel = game.add.text(game.width/2, game.height/2, 'Congratulations!', { font: "50px 'font-effect-shadow-multiple'", fill: '#ffffff' }); 
        nameLabel.anchor.setTo(0.5, 0.5);
        // Show the score at the center of the screen 
        // Explain how to start the game 
        var startLabel = game.add.text(game.width/2, game.height/2+60, 'press the up arrow key to Level 2', { font: '25px Arial', fill: '#ffffff' }); 
        startLabel.anchor.setTo(0.5, 0.5); 
        // Create a new Phaser keyboard variable: the up arrow key 
        // When pressed, call the 'start'
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP); 
        upKey.onDown.add(this.restart, this); 
    }, 
    restart: function() { 
        // Start the actual game 
        game.state.start('play'); 
    }, 
}; 