var play0State = {

    /// ToDo: Load some images in preload function.
	///		  1. Three images in asset directory
	///			 player.png, bg.png, tree2.png
    preload: function() {
        
    },

    create: function() {
        game.physics.startSystem(Phaser.Physics.ARCADE);

        this.starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield'); //bg
        this.bg_audio = game.add.audio('bg_audio');
        this.bg_audio.play();
        this.explosion = game.add.audio('explosion');
        this.blaster = game.add.audio('blaster');
        this.shot = game.add.audio('shot');

        this.player = game.add.sprite( game.width/2, 500, 'player');
        this.player.anchor.setTo(0.5, 0.5); //change anchor point’s position to center
        game.physics.arcade.enable(this.player);
        this.player.animations.add('fly', [0, 1, 2, 3], 20, true);
        this.player.play('fly');
        

        this.boss = game.add.sprite( game.width/2, 0, 'boss');
        this.boss.anchor.setTo(0.5, 1);
        game.physics.arcade.enable(this.boss);
        this.bossExists = false;
        this.boss.scale.setTo(0.5, 0.5);

        

        this.enemyPool = game.add.group();
        this.enemyPool.enableBody = true;
        this.enemyPool.createMultiple(20, 'enemy');
        this.enemyPool.setAll('anchor.x', 0.5);
        this.enemyPool.setAll('anchor.y', 0.5);
        game.time.events.loop(1000, this.createEnemy, this);
        /*this.enemyPool.forEach(function(enemy) {
            enemy.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
        }); 
        this.nextEnemyAppearTime = 0;
        this.enemyDelay = 2000;*/

        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(30, 'bullet');
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 1);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);
        this.bulletTime = 0;
        this.player.emitter = game.add.emitter(0, 0, 50);
        this.player.emitter.makeParticles('enemyBullet');
        this.player.emitter.setScale(1, 0, 1, 0, 1000);
        this.player.emitter.gravity = 0;


        this.explosions = game.add.group();

        for (var i = 0; i < 10; i++)
        {
            var explosionAnimation = this.explosions.create(0, 0, 'kaboom', [0], false);
            explosionAnimation.anchor.setTo(0.5, 0.5);
            explosionAnimation.animations.add('kaboom');
        }
        
        this.score = 0;
        this.scoreString = 'Score : ';
        this.scoreText = game.add.text(10, 10, this.scoreString + this.score, { font: '34px Arial', fill: '#ffffff' });

        this.lives = game.add.group();
        game.add.text(game.world.width - 100, 10, 'Lives : ', { font: '34px Arial', fill: '#ffffff' });


        for (var i = 0; i < 3; i++) {
            var player = this.lives.create(game.world.width - 100 + (30 * i), 60, 'lives');
            player.anchor.setTo(0.5, 0.5);
            player.angle = 90;
            player.alpha = 0.4;
        }

        this.cursor = game.input.keyboard.createCursorKeys();
        this.spaceBtn = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    },

    update: function(){
        this.starfield.tilePosition.y += 2; // scroll bg

        if (this.cursor.left.isDown){
            this.player.x -= 10;
        }
        else if(this.cursor.right.isDown){
            this.player.x += 10;;
        }
        if(this.spaceBtn.isDown){
            this.fireBullet();
        }

        game.physics.arcade.overlap(this.bullets, this.enemyPool, this.hitEnemy, null, this);
        game.physics.arcade.overlap(this.player, this.enemyPool,this.enemyHitsPlayer, null, this);
       
    },
    createEnemy: function() {
        var enemy = this.enemyPool.getFirstDead();
        if (!enemy) return;
        
        enemy.anchor.setTo(0.5, 1);
        enemy.reset(game.rnd.integerInRange(20, 780), 0);
        enemy.body.velocity.y = game.rnd.integerInRange(100, 200);
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
        enemy.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
        enemy.animations.play('fly');

    },



    fireBullet: function(enemy, bullet) {
        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > this.bulletTime)
        {
            //  Grab the first bullet we can from the pool
            bullet = this.bullets.getFirstExists(false);
            if (bullet)
            {
                //  And fire it
                bullet.reset(this.player.x, this.player.y + 8);
                bullet.body.velocity.y = -400;
                

                this.bulletTime = game.time.now + 200;
                this.blaster.play();
            }
        }
        
    
    },

    hitEnemy: function(enemy, bullet) {
     
        bullet.kill();
        enemy.kill();
        this.shot.play();
        
        this.score += 20;
        this.scoreText.text = this.scoreString + this.score;

        var explosion = this.explosions.getFirstExists(false);
        explosion.reset(enemy.body.x, enemy.body.y);
        explosion.play('kaboom', 30, false, true);
        console.log(this.score);
        if(this.score >= 300){
            game.state.start('win');
        }
    },
    enemyHitsPlayer: function(player,bullet) {
        
        bullet.kill();
        this.explosion.play();

        live = this.lives.getFirstAlive();
    
        if (live)
        {
            live.kill();
        }
    
        //  And create an explosion :)
        this.player.emitter.x = this.player.x; 
        this.player.emitter.y = this.player.y; 
        this.player.emitter.start(true, 800, null, 15);
        //var explosion = this.explosions.getFirstExists(false);
        //explosion.reset(player.body.x, player.body.y);
        //explosion.play('kaboom', 30, false, true);
    
        // When the player dies
        if (this.lives.countLiving() < 1)
        {
            player.kill();
            
            game.global.score = this.score;
            if(this.score > game.global.bestscore)
                game.global.bestscore = this.score;
            game.time.events.add(1000, function() {game.state.start('dead');}, this);

        }
    
    }
    
};
