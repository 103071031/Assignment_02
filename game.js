// Initialize Phaser 
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas'); 
// Define our global variable 
game.global = { score: 0 , bestscore: 0, bestscore_2p: 0}; 
// Add all the states 
game.state.add('boot', bootState); 
game.state.add('load', loadState); 
game.state.add('menu', menuState); 
game.state.add('play', playState);
game.state.add('play0', play0State);
game.state.add('play_2p', playState_2p);
game.state.add('win', winState);
game.state.add('dead', deadState); 
// Start the 'boot' state 
game.state.start('boot')