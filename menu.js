var menuState = { 
    create: function() {
        // Add a background image 
        // Display the name of the game 
        var nameLabel = game.add.text(game.width/2, 80, 'My Game', { font: "50px 'font-effect-shadow-multiple'", fill: '#ffffff' }); 
        nameLabel.anchor.setTo(0.5, 0.5);
        // Show the score at the center of the screen 
        var bestscoreLabel = game.add.text(game.width/2, game.height/2+60, 'Best score: ' + game.global.bestscore, { font: '25px Arial', fill: '#ffffff' });
        bestscoreLabel.anchor.setTo(0.5, 0.5); 
        var score_2pLabel = game.add.text(game.width/2, game.height/2+95, 'Best 2p score: ' + game.global.bestscore_2p, { font: '25px Arial', fill: '#ffffff' }); 
        score_2pLabel.anchor.setTo(0.5, 0.5);
        // Explain how to start the game  
        var startLabel = game.add.text(game.width/2, game.height-110, 'press the up arrow key to start 1p', { font: '25px Arial', fill: '#ffffff' }); 
        startLabel.anchor.setTo(0.5, 0.5); 
        var startLabel = game.add.text(game.width/2, game.height-80, 'press the down arrow key to start 2p', { font: '25px Arial', fill: '#ffffff' }); 
        startLabel.anchor.setTo(0.5, 0.5); 
        // Create a new Phaser keyboard variable: the up arrow key 
        // When pressed, call the 'start'
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP); 
        upKey.onDown.add(this.start, this); 
        var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN); 
        downKey.onDown.add(this.start_2p, this); 
    }, 
    start: function() { 
        // Start the actual game 
        game.state.start('play0'); 
    }, 
    start_2p: function() { 
        // Start the actual game 
        game.state.start('play_2p'); 
    }
}; 
