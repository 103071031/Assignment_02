var deadState = { 
    create: function() {
        // Add a background image 
        // Display the name of the game 
        var nameLabel = game.add.text(game.width/2, game.height/2, 'Game Over!', { font: "50px 'font-effect-shadow-multiple'", fill: '#ffffff' }); 
        nameLabel.anchor.setTo(0.5, 0.5);
        // Show the score at the center of the screen 
        var bestscoreLabel = game.add.text(game.width/2, game.height/2+60, 'Best score: ' + game.global.bestscore, { font: '25px Arial', fill: '#ffffff' });
        bestscoreLabel.anchor.setTo(0.5, 0.5); 
        var bestscore_2pLabel = game.add.text(game.width/2, game.height/2+95, 'Best 2p score: ' + game.global.bestscore_2p, { font: '25px Arial', fill: '#ffffff' });
        bestscore_2pLabel.anchor.setTo(0.5, 0.5); 
        var scoreLabel = game.add.text(game.width/2, game.height/2+130, 'Score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' }); 
        scoreLabel.anchor.setTo(0.5, 0.5);
        // Explain how to start the game 
        var startLabel = game.add.text(game.width/2, game.height/2+170, 'press the up arrow key back to menu', { font: '25px Arial', fill: '#ffffff' }); 
        startLabel.anchor.setTo(0.5, 0.5); 
        // Create a new Phaser keyboard variable: the up arrow key 
        // When pressed, call the 'start'
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP); 
        upKey.onDown.add(this.restart, this); 
    }, 
    restart: function() { 
        // Start the actual game 
        game.state.start('menu'); 
    }, 
}; 