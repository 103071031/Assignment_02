

var loadState = { 
    preload: function () {
        // Add a 'loading...' label on the screen 
        var loadingLabel = game.add.text(game.width/2, 150, 'loading...', { font: '30px Arial', fill: '#ffffff' }); 
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar 
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar'); 
        progressBar.anchor.setTo(0.5, 0.5); 
        game.load.setPreloadSprite(progressBar);
        // Load all game assets 
        game.load.image('bullet', 'assets/bullet1.png');
        game.load.image('enemyBullet', 'assets/bullet87.png');
        game.load.image('boss', 'assets/boss1.png');
        game.load.image('pixel', 'assets/bullet.png');
        game.load.spritesheet('enemy', 'assets/enemy.png', 32, 32);
        game.load.spritesheet('player', 'assets/player.png', 64, 64);
        game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
        game.load.image('starfield', 'assets/starfield.jpg',800,600);
        game.load.image('lives', 'assets/lives.png');
        game.load.audio('explosion', 'assets/explosion.mp3');
        game.load.audio('blaster', 'assets/blaster.mp3');
        game.load.audio('shot', 'assets/shot1.wav');
        game.load.audio('bg_audio', 'assets/bg.mp3');
        // Load a new asset that we will use in the menu state 
        }, 
        create: function() { 
            // Go to the menu state 
            game.state.start('menu'); 
        } 
};
