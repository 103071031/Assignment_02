
var playState_2p = {

    /// ToDo: Load some images in preload function.
	///		  1. Three images in asset directory
	///			 player.png, bg.png, tree2.png
    preload: function() {
        
    },

    create: function() {
        game.physics.startSystem(Phaser.Physics.ARCADE);

        this.starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield'); //bg
        this.bg_audio = game.add.audio('bg_audio');
        this.bg_audio.play();
        this.bg_audio.volume = 0.5;
        this.explosion = game.add.audio('explosion');
        this.blaster = game.add.audio('blaster');
        this.shot = game.add.audio('shot');

        this.player = game.add.sprite( 500, 500, 'player');
        this.player.anchor.setTo(0.5, 0.5); //change anchor point’s position to center
        game.physics.arcade.enable(this.player);
        this.player.animations.add('fly', [0, 1, 2, 3], 20, true);
        this.player.play('fly');
        
        this.player2 = game.add.sprite( 300, 500, 'lives');
        this.player2.anchor.setTo(0.5, 0.5); //change anchor point’s position to center
        game.physics.arcade.enable(this.player2);

        this.enemyPool = game.add.group();
        this.enemyPool.enableBody = true;
        this.enemyPool.createMultiple(20, 'enemy');
        this.enemyPool.setAll('anchor.x', 0.5);
        this.enemyPool.setAll('anchor.y', 0.5);
        game.time.events.loop(1000, this.createEnemy, this);
        /*this.enemyPool.forEach(function(enemy) {
            enemy.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
        }); 
        this.nextEnemyAppearTime = 0;
        this.enemyDelay = 2000;*/

        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(30, 'bullet');
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 1);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);
        this.bulletTime = 0;
        this.player.emitter = game.add.emitter(0, 0, 50);
        this.player.emitter.makeParticles('enemyBullet');
        this.player.emitter.setScale(1, 0, 1, 0, 1000);
        this.player.emitter.gravity = 0;

        

        //  By setting the min and max rotation to zero, you disable rotation on the particles fully
        

        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyBullets.createMultiple(30, 'enemyBullet');
        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 1);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);
        this.firingTimer = 0;
        this.enemies = [];

        this.enemiesTotal = 1;
        this.enemiesAlive = 1;
        this.nextEnemyAppearTime = 0;
        this.enemyDelay = 2000;

        /*for (var i = 0; i < 3; i++)
        {
            this.enemies.push(new Enemy(i, game, this.player, this.enemyBullets));
        }*/

        this.explosions = game.add.group();

        for (var i = 0; i < 10; i++)
        {
            var explosionAnimation = this.explosions.create(0, 0, 'kaboom', [0], false);
            explosionAnimation.anchor.setTo(0.5, 0.5);
            explosionAnimation.animations.add('kaboom');
        }
        
        this.score = 0;
        this.scoreString = 'Score : ';
        this.scoreText = game.add.text(10, 10, this.scoreString + this.score, { font: '34px Arial', fill: '#ffffff' });

        this.lives = game.add.group();
        game.add.text(game.world.width - 100, 10, '1p Lives : ', { font: '25px Arial', fill: '#ffffff' });
        this.lives2 = game.add.group();
        game.add.text(game.world.width - 100, 50, '2p Lives : ', { font: '25px Arial', fill: '#ffffff' });


        for (var i = 0; i < 3; i++) {
            var player = this.lives.create(game.world.width - 100 + (30 * i), 50, 'lives');
            player.anchor.setTo(0.5, 0.5);
            player.angle = 90;
            player.alpha = 0.4;
            var player2 = this.lives2.create(game.world.width - 100 + (30 * i), 90, 'lives');
            player2.anchor.setTo(0.5, 0.5);
            player2.angle = 90;
            player2.alpha = 0.4;
        }

        this.cursor = game.input.keyboard.createCursorKeys();
        this.spaceBtn = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.wasd = { 
            attack: game.input.keyboard.addKey(Phaser.Keyboard.R), 
            left: game.input.keyboard.addKey(Phaser.Keyboard.A), 
            right: game.input.keyboard.addKey(Phaser.Keyboard.D),
            up: game.input.keyboard.addKey(Phaser.Keyboard.W), 
            down: game.input.keyboard.addKey(Phaser.Keyboard.S)  
        };
    },

    update: function(){
        this.starfield.tilePosition.y += 2; // scroll bg

        if (this.cursor.left.isDown){
            this.player.x -= 10;
        }
        else if(this.cursor.right.isDown){
            this.player.x += 10;;
        }
        if(this.cursor.up.isDown){
            this.player.y -= 10;
        }
        else if(this.cursor.down.isDown){
            this.player.y += 10;
        }
        if(this.spaceBtn.isDown){
            this.fireBullet();
        }
        if (this.wasd.left.isDown){
            this.player2.x -= 10;
        }
        else if(this.wasd.right.isDown){
            this.player2.x += 10;;
        }
        if(this.wasd.up.isDown){
            this.player2.y -= 10;
        }
        else if(this.wasd.down.isDown){
            this.player2.y += 10;
        }
        if(this.wasd.attack.isDown){
            this.fireBullet2();
        }

        game.physics.arcade.overlap(this.bullets, this.enemyPool, this.hitEnemy, null, this);
        game.physics.arcade.overlap(this.enemyBullets, this.player, this.enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(this.player, this.enemyPool,this.enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(this.bullets, this.enemyPool, this.hitEnemy, null, this);
        game.physics.arcade.overlap(this.enemyBullets, this.player2, this.enemyHitsPlayer2, null, this);
        game.physics.arcade.overlap(this.player2, this.enemyPool,this.enemyHitsPlayer2, null, this);
    },
    createEnemy: function() {
        var enemy = this.enemyPool.getFirstDead();
        if (!enemy) return;
        
        enemy.anchor.setTo(0.5, 1);
        enemy.reset(game.rnd.integerInRange(20, 780), 0);
        enemy.body.velocity.y = game.rnd.integerInRange(50, 150);
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
        enemy.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
        enemy.animations.play('fly');

        game.time.events.loop(2500, function() {this.enemyFire(enemy);}, this);
    },

    enemyFire: function(enemy) {
        var enemyfire = this.enemyBullets.getFirstDead();
        if(!enemyfire || !enemy.alive) return;
        enemyfire.smoothed = false;
        enemyfire.anchor.setTo(0.5, 1);
        enemyfire.reset(enemy.x, enemy.y);
        enemyfire.body.velocity.y = game.rnd.integerInRange(160, 300);
        enemyfire.checkWorldBounds = true;
        enemyfire.outOfBoundsKill = true;
    },


    fireBullet: function(enemy, bullet) {
        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > this.bulletTime)
        {
            //  Grab the first bullet we can from the pool
            bullet = this.bullets.getFirstExists(false);
            if (bullet)
            {
                //  And fire it
                bullet.reset(this.player.x, this.player.y + 8);
                bullet.body.velocity.y = -400;
                

                this.bulletTime = game.time.now + 200;
                this.blaster.play();
            }
        }
        
    
    },
    fireBullet2: function(enemy, bullet) {
        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > this.bulletTime)
        {
            //  Grab the first bullet we can from the pool
            bullet = this.bullets.getFirstExists(false);
            if (bullet)
            {
                //  And fire it
                bullet.reset(this.player2.x, this.player2.y + 8);
                bullet.body.velocity.y = -400;
                

                this.bulletTime = game.time.now + 200;
                this.blaster.play();
            }
        }
        
    
    },

    hitEnemy: function(enemy, bullet) {
     
        bullet.kill();
        enemy.kill();
        this.shot.play();
        
        this.score += 100;
        this.scoreText.text = this.scoreString + this.score;

        var explosion = this.explosions.getFirstExists(false);
        explosion.reset(enemy.body.x, enemy.body.y);
        explosion.play('kaboom', 30, false, true);
    },
    enemyHitsPlayer: function(player,bullet) {
        
        bullet.kill();
        this.explosion.play();

        live = this.lives.getFirstAlive();
    
        if (live)
        {
            live.kill();
        }
    
        //  And create an explosion :)
        this.player.emitter.x = this.player.x; 
        this.player.emitter.y = this.player.y; 
        this.player.emitter.start(true, 800, null, 15);
        //var explosion = this.explosions.getFirstExists(false);
        //explosion.reset(player.body.x, player.body.y);
        //explosion.play('kaboom', 30, false, true);
    
        // When the player dies
        if (this.lives.countLiving() < 1)
        {
            player.kill();

            this.enemyBullets.callAll('kill');
            
            if(this.lives2.countLiving() < 1){
                game.global.score = this.score;
                if(this.score > game.global.bestscore_2p)
                    game.global.bestscore_2p = this.score;
                game.time.events.add(1000, function() {game.state.start('dead');}, this);
            }
        }
    
    },
    enemyHitsPlayer2: function(player,bullet) {
        
        bullet.kill();
        this.explosion.play();

        live = this.lives2.getFirstAlive();
    
        if (live)
        {
            live.kill();
        }
    
        //  And create an explosion :)
        this.player.emitter.x = this.player2.x; 
        this.player.emitter.y = this.player2.y; 
        this.player.emitter.start(true, 800, null, 15);
        //var explosion = this.explosions.getFirstExists(false);
        //explosion.reset(player.body.x, player.body.y);
        //explosion.play('kaboom', 30, false, true);
    
        // When the player dies
        if (this.lives2.countLiving() < 1)
        {
            player.kill();

            this.enemyBullets.callAll('kill');
            
            if(this.lives.countLiving() < 1){
                game.global.score = this.score;
                if(this.score > game.global.bestscore_2p)
                    game.global.bestscore_2p = this.score;
                game.time.events.add(1000, function() {game.state.start('dead');}, this);
            }

        }
    
    }
    
};