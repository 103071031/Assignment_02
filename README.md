# Software Studio 2019 Spring Assignment_02

## Topic
* Project Name : Raiden

## Website Detail Description

# 作品網址：https://103071031.gitlab.io/Assignment_02

# Components Description : 
1. Complete game process 
    * start menu :  上: 1p模式 下: 2p模式
    * game view 1P模式 : level 1: 左右鍵移動、SPACE鍵攻擊 level 2:上下左右鍵移動、SPACE攻擊
    * game view 2P模式 : player1: 上下左右鍵移動、SPACE鍵攻擊 player2: WASD移動、R攻擊 共同計分
    * game over : 按上回到menu
2. Basic rules 
    * player : 可以移動、射擊、有3條命，碰到敵人或被敵人射到減命
    * Enemy : 自動生成敵人，1p模式level2及2p模式會發出子彈
    * Map : 背景移動 
3. Jucify mechanisms 
    * Level : 1p模式 level 1達300分則進入level 2，level 1 僅玩家可射擊 但玩家碰到敵人則減命，level 2 敵人會發射子彈，玩家碰到敵人子彈或敵人本身皆會減命
4. Animations
    * player、enemy本身皆有animation
    * player射中enemy有爆炸的animation
5. Particle Systems
    * player 被敵人子彈射中或碰到敵人時有Particle Systems特效
6. Sound effects
    * 背景音樂、玩家射擊音效、敵人被射中爆炸音效、玩家死亡爆炸音效
7. UI
    * player health : 每次三條命(右上)
    * score(左上)，menu也會記錄最佳成績、2p最佳成績，game over時也會顯示本次遊戲成績
8. Appearance


# Bonus :  
1. Multi-player game : offline 2p

